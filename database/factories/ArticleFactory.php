<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'category_id' => rand(1, 5),
        'user_id' => rand(1, 5),
        'title' => $faker->text(30),
        'description' => $faker->paragraph,
    ];
});
