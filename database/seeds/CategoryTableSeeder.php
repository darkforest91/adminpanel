<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'title' => 'News'
        ]);

        DB::table('categories')->insert([
            'title' => 'Sport'
        ]);

        DB::table('categories')->insert([
            'title' => 'Education'
        ]);

        DB::table('categories')->insert([
            'title' => 'Cars'
        ]);

        DB::table('categories')->insert([
            'title' => 'Games'
        ]);


    }
}
