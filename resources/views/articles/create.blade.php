@extends('layouts.app')

@section('content')

    <h2>Create article</h2>

    <form method="post" action="{{route('articles.store')}}" class="w-50" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title">
        @error('title')
        <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" rows="3" name="description"></textarea>
    </div>

    <div class="form-group">
        <label for="category">Category</label>
        <select class="form-control" id="category" name="category_id">
            @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->title}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" class="form-control-file @error('image') is-invalid @enderror" id="image" name="image">
        @error('image')
        <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Create</button>
    </form>

    <div class="mt-3 mb-5">
        <a href="{{route('articles.index')}}">Back</a>
    </div>

@endsection
