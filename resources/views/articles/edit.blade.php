@extends('layouts.app')

@section('content')

    <h2>Edit article</h2>

    <form method="post" action="{{route('articles.update', ['article' => $article])}}" class="w-50" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{$article->title}}">
            @error('title')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" rows="3" name="description">{{$article->description}}</textarea>
        </div>

        <div class="form-group">
            <label for="category">Category</label>
            <select class="form-control" id="category" name="category_id">
                @foreach($categories as $category)
                    <option @if($category->id === $article->category->id) selected @endif value="{{$category->id}}">{{$category->title}}</option>
                @endforeach
            </select>
        </div>

        @if(!is_null($article->image))
            <img src="{{asset('storage/' . $article->image)}}" alt="{{$article->image}}" style="width:100px;height:100px;"><br/><br>
        @endif

        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" class="form-control-file @error('image') is-invalid @enderror" id="image" name="image" value="{{$article->image}}">
            @error('image')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>

    <div class="mt-3 mb-5">
        <a href="{{route('articles.index')}}">Back</a>
    </div>

@endsection
