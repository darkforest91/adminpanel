@extends('layouts.app')

@section('content')

    <h2>All articles</h2>
    <p><a href="{{route('articles.create')}}">Create article</a></p>

    <div class="row justify-content-between">
        @foreach($articles as $article)
            <div class="card mb-5" style="width: 15rem;">
                @if(!is_null($article->image))
                    <img src="{{asset('storage/' . $article->image)}}" class="card-img-top" alt="{{$article->image}}" style="height: 238px; width: 238px">
                @else
                    <img src="{{asset('img/no_img.jpg')}}" alt="No image" class="card-img-top">
                @endif
                <div class="card-body">
                    <h5 class="card-title"><a href="{{route('articles.show', ['article' => $article])}}">{{$article->title}}</a></h5>
                </div>
                    <p class="card-text text-right"><small class="text-muted">Category: {{$article->category->title}}</small></p>
                    <p class="card-text text-right"><small class="text-muted">Author: {{$article->user->name}}</small></p>
            </div>
        @endforeach
    </div>

    <div class="row p-5">
        <div class="col-12">
            {{ $articles->links() }}
        </div>
    </div>

@endsection
