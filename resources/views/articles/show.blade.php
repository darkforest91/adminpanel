@extends('layouts.app')

@section('content')

    <div class="card bg-light mb-3" style="max-width: 800px;">
        <div class="row no-gutters">
            <div class="col-md-4">
                @if(!is_null($article->image))
                    <img src="{{asset('storage/' . $article->image)}}" class="card-img" alt="{{$article->image}}" style="">
                @else
                    <img src="{{asset('img/no_img.jpg')}}" alt="No image" class="card-img">
                @endif
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{$article->title}}</h5>
                    <p class="card-text">{{$article->description}}</p>
                    <p class="card-text text-right"><small class="text-muted">Category: {{$article->category->title}}</small></p>
                    <p class="card-text text-right"><small class="text-muted">Author: {{$article->user->name}}</small></p>
                </div>
            </div>
        </div>
    </div>

    <div>
        <p><a href="{{route('articles.edit', ['article' => $article])}}">Edit</a></p>
        <form method="post" action="{{route('articles.destroy', ['article' => $article])}}">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
        </form>
    </div>

    <div class="mt-5 mb-5">
        <a href="{{route('articles.index')}}">Back</a>
    </div>

@endsection
