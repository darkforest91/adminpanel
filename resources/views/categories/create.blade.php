@extends('layouts.app')

@section('content')

    <h2>Create category</h2>

    <form method="post" action="{{route('categories.store')}}" class="w-50">
        @csrf

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title">
            @error('title')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="parent_id">Parent</label>
            <select class="form-control" id="parent_id" name="parent_id">
                <option class="text-muted" value="{{null}}"></option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->title}}</option>
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-success">Create</button>
    </form>

    <p class="mt-4"><a href="{{route('categories.index')}}">Back</a></p>

@endsection
