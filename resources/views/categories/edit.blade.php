@extends('layouts.app')

@section('content')

    <h2>Edit category</h2>

    <form method="post" action="{{route('categories.update', ['category' => $category])}}" class="w-50">
        @method('put')
        @csrf

        <div class="form-group">
            <label for="title">User name</label>
            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{$category->title}}">
            @error('title')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="parent_id">Parent</label>
            <select class="form-control" id="parent_id" name="parent_id">
                <option class="text-muted" value="{{null}}"></option>
                @foreach($categories as $select_category)
                    @if(is_null($category->parent))
                        <option value="{{$select_category->id}}">
                            {{$select_category->title}}
                        </option>
                    @else
                        <option @if($category->parent->id == $select_category->id) selected @endIf value="{{$select_category->id}}">
                            {{$select_category->title}}
                        </option>
                    @endif
                @endforeach
            </select>
        </div>

        <button type="submit" class="btn btn-success">Edit</button>
    </form>

    <p class="mt-4"><a href="{{route('categories.index')}}">Back</a></p>

@endsection
