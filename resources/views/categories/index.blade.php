@extends('layouts.app')

@section('content')

    <h2>All categories:</h2>
    <p><a href="{{route('categories.create')}}">Create new category</a></p>

    <table class="table w-50">
        <thead class="thead-light">
        <tr>
            <th scope="col">Title</th>
            <th scope="col" colspan="2">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category->title}}</td>
                <td><a href="{{route('categories.edit', ['category' => $category])}}">Edit</a></td>
                <td>
                    <form method="post" action="{{route('categories.destroy', ['category' => $category])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
