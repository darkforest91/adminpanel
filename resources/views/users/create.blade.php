@extends('layouts.app')

@section('content')

    <h2>Create user</h2>

    <form method="post" action="{{route('users.store')}}" class="w-50">
        @csrf

        <div class="form-group">
            <label for="name">User name</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name">
            @error('name')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">Password</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                @error('password')
                <p class="text-danger">{{ $message }}</p>
                @enderror
        </div>

        <button type="submit" class="btn btn-success">Create</button>
    </form>

    <p class="mt-4"><a href="{{route('users.index')}}">Back</a></p>

@endsection
