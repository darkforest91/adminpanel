@extends('layouts.app')

@section('content')

    <h2>Edit user</h2>

    <form method="post" action="{{route('users.update', ['user' => $user])}}" class="w-50">
        @method('put')
        @csrf

        <div class="form-group">
            <label for="name">User name</label>
            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{$user->name}}">
            @error('name')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{$user->password}}">
            @error('password')
            <p class="text-danger">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="btn btn-success">Edit</button>
    </form>

    <p class="mt-4"><a href="{{route('users.index')}}">Back</a></p>

@endsection
