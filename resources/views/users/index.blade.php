@extends('layouts.app')

@section('content')

    <h2>All users:</h2>
    <p><a href="{{route('users.create')}}">Create new user</a></p>

    <table class="table w-50">
        <thead class="thead-light">
        <tr>
            <th scope="col">Name</th>
            <th scope="col" colspan="2">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td><a href="{{route('users.show', ['user' => $user])}}">{{$user->name}}</a></td>
                <td><a href="{{route('users.edit', ['user' => $user])}}">Edit</a></td>
                <td>
                    <form method="post" action="{{route('users.destroy', ['user' => $user])}}">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
