@extends('layouts.app')

@section('content')

    <h2 class="mb-4">User "{{$user->name}}"</h2>
    <div>
        <p>Name: {{$user->name}}</p>
        <p>Password(encrypted): {{$user->password}}</p>
    </div>

    <div>
        <a href="{{route('users.edit', ['user' => $user])}}">Edit</a>
        <form method="post" action="{{route('users.destroy', ['user' => $user])}}">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-outline-danger btn-sm">Удалить</button>
        </form>
    </div>

    <p class="mt-5"><a href="{{route('users.index')}}">Back</a></p>


@endsection
