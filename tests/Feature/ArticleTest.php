<?php

namespace Tests\Feature;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    private $user, $category, $articles;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->category = factory(Category::class)->create();
        $this->articles = factory(Article::class, 3)->create([
            'user_id' => $this->user->id,
            'category_id' => $this->category->id
        ]);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessGetArticles()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('articles.index'));
        $response->assertStatus(200);
        $response->assertSeeText('All articles');
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedGetArticles()
    {
        $response = $this->get(route('articles.index'));
        $response->assertDontSeeText('All articles');
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessGetViewCreateArticles()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('articles.create'));
        $response->assertStatus(200);
        $response->assertSeeText('Create article');
        $response->assertSeeText('Title');
        $response->assertSeeText('Description');
        $response->assertSeeText('Category');
        $response->assertSeeText('Image');
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedGetViewCreateArticles()
    {
        $response = $this->get(route('articles.create'));
        $response->assertRedirect(route('login'));
        $response->assertDontSeeText('Create article');
        $response->assertDontSeeText('Title');
        $response->assertDontSeeText('Description');
        $response->assertDontSeeText('Category');
        $response->assertDontSeeText('Image');
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessCreateArticle()
    {
        $this->actingAs($this->user);
        $article = [
            'category_id' => $this->category->id,
            'user_id' => $this->user->id,
            'title' => 'Test title',
            'description' => 'Test description',
        ];
        $response = $this->post(route('articles.store', $article));
        $response->assertRedirect(route('articles.index'));
        $this->assertDatabaseHas('articles', $article);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedCreateArticle()
    {
        $article = [
            'category_id' => $this->category->id,
            'user_id' => $this->user->id,
            'title' => 'Test title',
            'description' => 'Test description',
        ];
        $response = $this->post(route('articles.store', $article));
        $response->assertRedirect(route('login'));
        $this->assertDatabaseMissing('articles', $article);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessShowArticle()
    {
        $this->actingAs($this->user);
        $article = $this->articles->first();
        $response = $this->get(route('articles.show', ['article' => $article]));
        $response->assertStatus(200);
        $response->assertSeeText($article->title);
        $response->assertSeeText($article->description);
        $response->assertSeeText($article->user->name);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedShowArticle()
    {
        $article = $this->articles->first();
        $response = $this->get(route('articles.show', ['article' => $article]));
        $response->assertRedirect(route('login'));
        $response->assertDontSeeText($article->title);
        $response->assertDontSeeText($article->description);
        $response->assertDontSeeText($article->user->name);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessGetViewEditArticles()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('articles.edit', ['article' => $this->articles->first()]));
        $response->assertStatus(200);
        $response->assertSeeText('Edit article');
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedGetViewEditArticles()
    {
        $response = $this->get(route('articles.edit', ['article' => $this->articles->first()]));
        $response->assertRedirect(route('login'));
        $response->assertDontSeeText('Edit article');
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessUpdateArticle()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => 'test',
            'description' => 'test description',
            'user_id' => $this->user->id,
            'category_id' => $this->category->id
        ];
        $response = $this->put(route('articles.update', ['article' => $this->articles->first()]), $data);
        $response->assertRedirect(route('articles.index'));
        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedUpdateArticle()
    {
        $data = [
            'title' => 'test',
            'description' => 'test description',
            'user_id' => $this->user->id,
            'category_id' => $this->category->id
        ];
        $response = $this->put(route('articles.update', ['article' => $this->articles->first()]), $data);
        $response->assertRedirect(route('login'));
        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testSuccessDeleteArticle()
    {
        $this->actingAs($this->user);
        $response = $this->delete(route('articles.destroy', ['article' => $this->articles->first()]));
        $response->assertRedirect(route('articles.index'));
        $this->assertDatabaseMissing('articles', $this->articles->first()->toArray());
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function testFailedDeleteArticle()
    {
        $response = $this->delete(route('articles.destroy', ['article' => $this->articles->first()]));
        $response->assertRedirect(route('login'));
    }

}
