<?php

namespace Tests\Feature;

use App\Category;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    private $user, $categories;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->categories = factory(Category::class, 3)->create([
            'parent_id' => null
        ]);
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testSuccessGetCategories()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('categories.index'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testFailedGetCategories()
    {
        $response = $this->get(route('categories.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testSuccessGetViewCreateCategories()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('categories.create'));
        $response->assertStatus(200);
        $response->assertSeeText('Create category');
        $response->assertSeeText('Title');
        $response->assertSeeText('Parent');
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testFailedGetViewCreateCategories()
    {
        $response = $this->get(route('categories.create'));
        $response->assertRedirect(route('login'));
        $response->assertDontSeeText('Create category');
        $response->assertDontSee('Title');
        $response->assertDontSeeText('Subcategory');
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testGetSuccessViewEditCategories()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('categories.edit', ['category' => $this->categories->first()]));
        $response->assertStatus(200);
        $response->assertSeeText('Edit category');
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testGetFailedViewEditCategories()
    {
        $response = $this->get(route('categories.edit', ['category' => $this->categories->first()]));
        $response->assertRedirect(route('login'));
        $response->assertDontSeeText('Edit category');
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testSuccessCreateCategory()
    {
        $this->actingAs($this->user);
        $category = [
            'title' => 'Test title'
        ];
        $response = $this->post(route('categories.store', $category));
        $response->assertRedirect(route('categories.index'));
        $this->assertDatabaseHas('categories', $category);
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testFailedCreateCategory()
    {
        $category = [
            'title' => 'Test title'
        ];
        $response = $this->post(route('categories.store', $category));
        $response->assertRedirect(route('login'));
        $this->assertDatabaseMissing('categories', $category);
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testSuccessUpdateCategories()
    {
        $this->actingAs($this->user);
        $category = $this->categories->first();
        $category->title = 'Update title';
        $response = $this->put(route('categories.update', ['category' => $category]), $category->toArray());
        $response->assertRedirect(route('categories.index'));
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testFailedUpdateCategories()
    {
        $category = $this->categories->first();
        $category->title = 'Update title';
        $response = $this->put(route('categories.update', ['category' => $category]), $category->toArray());
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testSuccessDeleteCategory()
    {
        $this->actingAs($this->user);
        $response = $this->delete(route('categories.destroy', ['category' => $this->categories->first()]));
        $response->assertRedirect(route('categories.index'));
        $this->assertDatabaseMissing('categories', $this->categories->first()->toArray());
    }

    /**
     * A basic feature test example.
     * @group categories
     * @return void
     */
    public function testFailedDeleteCategory()
    {
        $response = $this->delete(route('categories.destroy', ['category' => $this->categories->first()]));
        $response->assertRedirect(route('login'));
    }
}
