<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testSuccessGetUsers()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('users.index'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testFailedGetUsers()
    {
        $response = $this->get(route('users.index'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testSuccessDeleteUser()
    {
        $this->actingAs($this->user);
        $new_user = factory(User::class)->create();
        $response = $this->delete(route('users.destroy', ['user' => $new_user]));
        $response->assertStatus(302);
        $response->assertRedirect(route('users.index'));
        $this->assertDatabaseMissing('users', $new_user->toArray());
    }

    /**
     * A basic feature test example.
     * @group users
     * @return void
     */
    public function testFailedDeleteUser()
    {
        $new_user = factory(User::class)->create();
        $response = $this->delete(route('users.destroy', ['user' => $new_user]));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }
}
